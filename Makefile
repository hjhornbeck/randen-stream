#!/usr/bin/make -w

OPT_FLAGS=-O2 -march=native

bin/randen_stream: main.c src/randen.c src/randen.h src/private/randen_round_keys.h src/private/randen_vector128.h
	gcc ${OPT_FLAGS} -o bin/randen_stream main.c src/randen.c
