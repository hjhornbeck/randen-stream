# Randen Stream

Tests the Randen CSPRNG by taking in a seed and outputting an infinite stream of bytes to STDOUT.
The seed can be supplied either by supplying an integer on the command line, or via a byte sequence fed into STDIN.
If both are supplied, the command line integer takes precidence.

## Examples

```
$ bin/randen_stream 42 | dd bs=4k count=256k status=progress | sha256sum -
Using 42 as a seed value.
Reorganizing seed values to match the recommendations.
979935232 bytes (980 MB, 935 MiB) copied, 9 s, 109 MB/s
262144+0 records in
262144+0 records out
1073741824 bytes (1.1 GB, 1.0 GiB) copied, 9.85628 s, 109 MB/s
c65e05e7e070b1f59091126f53699c6c0c4e003fbb3e254d64a88ca57f220e1a  -
                                                                                                                                                                                                           
$ bin/randen_stream 42 0 0 | dd bs=4k count=256k status=progress | sha256sum -
Using 42, 0, 0 as seed values.
Reorganizing seed values to match the recommendations.
980267008 bytes (980 MB, 935 MiB) copied, 9 s, 109 MB/s
262144+0 records in
262144+0 records out
1073741824 bytes (1.1 GB, 1.0 GiB) copied, 9.85919 s, 109 MB/s
c65e05e7e070b1f59091126f53699c6c0c4e003fbb3e254d64a88ca57f220e1a  -
```

Note that the actual seed is initialized to zeros, so trailing zeros have no effect.

```
$ dd if=/dev/urandom bs=32 count=1 | bin/randen_stream | dd bs=4k count=256k status=progress | sha256sum -
1+0 records in
1+0 records out
32 bytes copied, 1.1375e-05 s, 2.8 MB/s
Using 32 bytes read from STDIN as a seed value.
Reorganizing seed values to match the recommendations.
979537920 bytes (980 MB, 934 MiB) copied, 9 s, 109 MB/s
262144+0 records in
262144+0 records out
1073741824 bytes (1.1 GB, 1.0 GiB) copied, 9.88817 s, 109 MB/s
280827e0bb9a4c5eeaae1ee19c1684ab3f89157f09ce15414cecc8a85ba7345c  -

$ bin/randen_stream <<EOL | dd bs=4k count=256k status=progress | sha256sum -
I'm using plain text input, entered by the user, as a random seed! Note that there's
enough text here that the code doesn't have to be stingy when constructing the seed
value.
EOL
Using 177 bytes read from STDIN as a seed value.
976998400 bytes (977 MB, 932 MiB) copied, 9 s, 109 MB/s
262144+0 records in
262144+0 records out
1073741824 bytes (1.1 GB, 1.0 GiB) copied, 9.87496 s, 109 MB/s
2b8758858495972ae960c9eed561e73c76de162e922414ed14cdae3e19ebcb9a  -
```

## Credits

This is a fork of [Frank Denis' C port](https://github.com/jedisct1/randen-rng)
of the [Randen CSPRNG](https://github.com/google/randen), and carries the same license.
