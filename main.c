#include "src/randen.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

int main( int argc, char** argv ) {

	uint8_t buffer[RANDEN_SEED_BYTES];	// seed values, stored in linear fashion
	uint64_t buffer_size = 0;		// track the size of the buffer here
	uint8_t seed[RANDEN_SEED_BYTES];	// the seed, but (potentially) reorganized as per the specs
	RandenState state;			// Randen's internal state
	uint8_t result;				// byte output of Randen

	// explicitly blank out every byte of the seed, to make coding much easier
	memset( seed, 0, sizeof(seed) );

	// read in the seed value from the command line, if given
	if( argc >= 2 ) {

		fprintf( stderr, "Using " );

		// allow up to RANDEN_SEED_BYTES bytes as a seed
		for( int arg = 1; (arg < argc) && (arg < 1+RANDEN_SEED_BYTES); arg++ ) {

			if( arg != 1 )		// nicely format the output
				fprintf( stderr, ", " );

			buffer[buffer_size] = atoi( argv[arg] ) & 0xff;
			fprintf( stderr, "%d", buffer[buffer_size] );

			buffer_size++;
			}

		fprintf( stderr, " as " );
		if( argc == 2 )
			fprintf( stderr, "a seed value.\n" );
		else
			fprintf( stderr, "seed values.\n" );

		}
	// otherwise, attempt to read from STDIN
	else {
		buffer_size = fread( buffer, sizeof(buffer[0]), sizeof(buffer), stdin );
		fprintf( stderr, "Using %lld bytes read from STDIN as a seed value.\n", buffer_size );

		}

	// The docs recommend writing to indices 32-47 and 64-79, if only 32 bytes are input
	//  So divide into two cases: buffer_size < 80, which prioritizes those indicies...
	if( buffer_size < 80 ) {

		fprintf( stderr, "Reorganizing seed values to match the recommendations.\n" );
		for( int idx = 0; (idx < buffer_size) && (idx < 16); idx++ )
			seed[32 + idx] = buffer[idx];
		for( int idx = 16; (idx < buffer_size) && (idx < 32); idx++ )
			seed[64 - 16 + idx] = buffer[idx];
		for( int idx = 32; (idx < buffer_size) && (idx < 64); idx++ )
			seed[idx-32] = buffer[idx];
		for( int idx = 64; (idx < buffer_size) && (idx < 80); idx++ )
			seed[48 + idx - 64] = buffer[idx];

		}
	// or buffer_size >= 80, which doesn't have to prioritize 
	else {
		for( int idx = 0; idx < buffer_size; idx++ )
			seed[idx] = buffer[idx];
		}

	// initialize Randen
	randen_init( &state, seed );

	// now write to STDOUT in an infinite loop
	while( 1 ) {

		result = randen_generate_byte( &state );
		fwrite( &result, sizeof(result), 1, stdout );
		}

	}
